import csv

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse, FileResponse
from time import sleep

from django.conf import settings
from time import sleep

import redis
from django.conf import settings
from django.core.cache import cache
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic import ListView
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic.base import View

from home.emails import send_email
from home.forms import StudentForm, UserSignUpForm
from home.models import Student, Teacher, Subject


class SignUpView(View):

    def get(self, request):
        sign_up_form = UserSignUpForm()

        return render(request, 'sign_up.html', context={
            'form': sign_up_form,
        })

    def post(self, request):
        sign_up_form = UserSignUpForm(request.POST)
        if sign_up_form.is_valid():
            user = sign_up_form.save()
            user.is_active = False
            user.set_password(request.POST['password1'])
            user.save()

            uid = urlsafe_base64_encode(force_bytes(user.pk))

            activate_url = "{}/{}/{}".format(
                "http://localhost:8000/activate",
                uid,
                default_token_generator.make_token(user=user)
            )

            send_email(
                recipient_list=[user.email],
                activate_url=activate_url
            )

            return HttpResponse("Check your email list to activate account!")
        return HttpResponse("Wrong Data")


class ActivateView(View):

    def get(self, request, uid, token):

        user_id = force_bytes(urlsafe_base64_decode(uid))

        user = User.objects.get(pk=user_id)


        if not user.is_active and default_token_generator.check_token(user, token):

            user.is_active = True
            user.save()

            login(request, user)

            return HttpResponse('token checked')

        return HttpResponse('Your account activated')


class SignInView(View):

    def get(self, request):
        auth_form = AuthenticationForm()

        return render(request, 'sign_in.html', context={
            'form': auth_form,
        })

    def post(self, request):

        auth_form = AuthenticationForm(request.POST)
        # if auth_form.is_valid():
        user_a = authenticate(request=request,
                              username=request.POST.get('username'),
                              password=request.POST.get('password'))
        login(request, user_a)

        return redirect('/')
        # return HttpResponse('Wrong data {} !'.format(auth_form.error_messages))


class SignOutView(View):

    def get(self, request):
        logout(request)
        print(request.user)
        return HttpResponse('Logouted')


# @method_decorator(cache_page(settings.CACHE_TTL), name='dispatch')
class StudentView(View):

    def get(self, request):
        """
        Show all students in template
        """
        # В основном для кеширование используют вилку
        # проверяя есть ли значение в кэше и если есть,
        # то взять если нет то сохранить и взять
        cache_value = cache.get('some_key')
        if not cache_value:
            cache_value = "Cached value"
            cache.set('some_key', cache_value)

        # выбрать всех студентов из таблицы students(джанго сама
        # дописывает `s` в окончании таблицы, но модели всегда должны
        # быть в единственном числе)
        students = Student.objects.\
            select_related('book', 'subject').\
            prefetch_related('teachers').all()

        # для рендеринга темплейта нужно использовать render
        # эта функция принимает минимум два параметра request, template_name
        return render(
            request=request,
            template_name='student_list.html',
            context={
                'students': students,
                'cached_value': cache_value,
            }
        )


class StudentAddView(View):

    def get(self, request):
        """
        Create student by student's name
        """
        # все GET параметры хранятся в request.GET
        student_name_from_request = request.GET.get('name')

        if not student_name_from_request:
            return HttpResponse('Student name missing')

        # Чтобы создать нового юзера нужно инициализировать Модель
        student = Student()
        # далее сохраняем параметры в объект модели
        student.name = student_name_from_request
        # сохранение
        student.save()

        return HttpResponse('Student {} have been created'.format(student.name))


class StudentCreateView(View):

    def get(self, request):
        """
        Create student by Django Forms
        """
        # генерируем форму
        student_form = StudentForm()

        # добавляем форму в контекст
        context = {
            'student_form': student_form,
        }

        return render(request, 'student_form.html', context=context)

    def post(self, request):
        # получаем данные с формы
        student_form = StudentForm(request.POST)
        # сохраняем данные в таблицу
        # если все валидно
        if student_form.is_valid():
            student_form.save()

        return redirect(reverse('class_students_list'))


class StudentUpdateView(View):

    def get(self, request, id):
        """
        Create student by Django Forms
        """
        student = get_object_or_404(Student, id=id)

        # генерируем форму
        student_form = StudentForm(instance=student)

        # добавляем форму в контекст
        context = {
            'student_form': student_form,
            'student': student,
        }

        return render(request, 'student_update.html', context=context)

    def post(self, request, id):
        student = get_object_or_404(Student, id=id)

        # получаем данные с формы
        student_form = StudentForm(request.POST, instance=student)
        # сохраняем данные в таблицу
        # если все валидно
        if student_form.is_valid():
            student_form.save()

        return redirect(reverse('class_students_list'))


class JsonView(View):

    def get(self, request):
        # взятие списка студентов
        students = Student.objects.all()
        # взятие одного студента
        student = Student.objects.last()

        return JsonResponse({
            "message": "Hello World!",
            "student": model_to_dict(student),  # через model_to_dict можно конвертировать инстанс модели в словарь
            "students": list(students.values(  # через values можно конвертировать инстансы модели в словари
                "name",
                "book__title",
                "subject__title",
            )),
        })


class CSVView(View):

    def get(self, request):
        response = HttpResponse(content_type="text/csv")

        response['Content-Disposition'] = "attachment; filename=data_students.csv"

        # Создаем обертку на респонсом для записи данных
        writer_for_response = csv.writer(response)
        # Первая строка это строка Головы (информации об столбиках)
        writer_for_response.writerow(["Name", "Book", "Subject"])

        students = Student.objects.all()
        for student in students:
            # Остальные столбики записываются как тело CSV файла
            writer_for_response.writerow([
                student.name,
                student.book.title if student.book else None,
                student.subject.title if student.subject else None,
            ])

        return response


class FileView(View):

    def get(self, request):

        with open("pyproject.toml") as file:
            response = FileResponse(file.read())
            # Для выгрузки файла нужно указывать Content-Disposition с первым
            # параметром attachment и вторым названием файла
            response['Content-Disposition'] = "attachment; filename={}".format(file.name)

            return response


class XMLView(View):

    def get(self, request):
        # отображение XML файла происходит также как и обычных файлов, только можно дописывать тип файл
        with open("test_xml.xml") as file:
            response = FileResponse(file.read())

            response['Content-Type'] = 'text/xml'

            return response


class TeacherView(ListView):

    model = Teacher
    template_name = 'teacher.html'


class SubjectView(ListView):

    model = Subject
    template_name = 'subject.html'


class TeacherDetailView(DetailView):

    model = Teacher
    template_name = 'teacher_detail.html'


class TeacherCreateView(CreateView):

    model = Teacher
    fields = ['name']
    template_name = 'teacher_create.html'
    success_url = reverse_lazy('teacher_view')


class TeacherUpdateView(UpdateView):

    model = Teacher
    fields = ['name']
    template_name = 'teacher_update.html'


class TeacherDeleteView(DeleteView):

    model = Teacher
    success_url = reverse_lazy('teacher_view')
    template_name = 'teacher_delete.html'
